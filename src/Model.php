<?php

declare(strict_types=1);

namespace Heitus\Support;

use Carbon\Carbon;
use DateTimeInterface;
use Exception;
use Heitus\Support\Contracts\IModel;
use Heitus\Support\Exceptions\Contracts\IModelException;
use Heitus\Support\Exceptions\ModelException;
use Heitus\Support\Exceptions\ModelPropertyNotFoundException;
use ReflectionClass;
use ReflectionException;
use stdClass;

abstract class Model implements IModel
{

    protected array $_dates = ['createdAt', 'updatedAt', 'deletedAt'];

    private array $_defaultValues = [];
    private bool $_isDirty = true;
    private array $_dirtyFields = [];

    protected ?Carbon $createdAt = null;
    protected ?Carbon $updatedAt = null;
    protected ?Carbon $deletedAt = null;

    /**
     * BaseModel constructor.
     * @param $data
     * @throws ModelException
     * @throws ModelPropertyNotFoundException
     * @throws ReflectionException
     */
    public function __construct($data)
    {
        $this->build($data, true);
        $this->setClean();
    }

    /**
     * @param array|stdClass $data
     * @return IModel
     * @throws ModelException
     * @throws ModelPropertyNotFoundException
     * @throws ReflectionException
     */
    public function fill($data): IModel
    {
        /**
         * Call the model construction process, but indicating that we
         * are not coming from the model constructor
         */
        return $this->build($data, false);
    }

    /**
     * @param $data
     * @param bool $fromConstructor
     * @return IModel
     * @throws ModelException
     * @throws ModelPropertyNotFoundException
     * @throws ReflectionException
     */
    private function build($data, bool $fromConstructor): IModel
    {
        if (!is_array($data) && !is_object($data)) {
            throw new ModelException(
                "Invalid data passed to model: " . $data,
                IModelException::INVALID_DATA
            );
        }

        $this->mapDataToModelProperties((object)$data, $fromConstructor);
        $this->checkIntegrity();
        return $this;
    }

    /**
     * @param object $data
     * @param bool $fromConstructor
     * @throws ModelException
     * @throws ModelPropertyNotFoundException
     */
    private function mapDataToModelProperties(object $data, bool $fromConstructor)
    {
        foreach ($data as $property => $value) {
            $property = camel_case($property);
            if (!$this->hasProperty($property)) {
                $this->throwPropertyNotFoundException($property);
            }

            if (in_array($property, $this->_dates)) {
                $value = $this->parseDate($property, $value);
                if ($fromConstructor === false && $value === null && $this->$property === null) {
                    continue;
                } elseif ($fromConstructor === false && $value !== null && $value->eq($this->$property)) {
                    continue;
                }
            } else {
                $value = $this->parseFieldWhenSetting($property, $value);
                if ($fromConstructor === false && $this->$property === $value) {
                    continue;
                }
            }

            if ($fromConstructor === false && !isset($this->_dirtyFields[$property])) {
                $this->_defaultValues[$property] = $this->$property;
                $this->_dirtyFields[$property] = 1;
                $this->_isDirty = true;
            }
            $this->$property = $value;
        }
    }

    /**
     * @param string $propertyName
     * @return bool
     */
    public function hasProperty(string $propertyName): bool
    {
        return property_exists($this, camel_case($propertyName));
    }

    /**
     * @param string $property
     * @throws ModelPropertyNotFoundException
     */
    private function throwPropertyNotFoundException(string $property): void
    {
        throw new ModelPropertyNotFoundException(
            "Model property '" . $property . "' doesn't exists on '" . get_class_short_name(static::class) . "'"
        );
    }

    /**
     * @param $property
     * @param $value
     * @return DateTimeInterface|null
     * @throws ModelException
     */
    private function parseDate($property, $value): ?DateTimeInterface
    {
        // we want to consider an empty string as null
        if (is_string($value) && empty($value)) {
            $value = null;
        }
        if (!$value instanceof DateTimeInterface && $value !== null) {
            try {
                return Carbon::parse($value)->startOfSecond();
            } catch (Exception $e) {
                throw new ModelException(
                    "Invalid field '" . $property . "' value: " . $value,
                    ModelException::INVALID_DATA,
                    ['invalid_' . snake_case($property) . '_format'],
                    $e
                );
            }
        } else {
            return $value;
        }
    }

    /**
     * @param string $field
     * @param $value
     * @return mixed
     */
    public function parseFieldWhenSetting(string $field, $value)
    {
        return $value;
    }

    /**
     * @throws ModelException
     * @throws ReflectionException
     */
    private function checkIntegrity()
    {
        $this->checkPropertiesInitialization();
        $this->validatePropertiesValues();
    }

    /**
     * @throws ModelException
     * @throws ReflectionException
     */
    private function checkPropertiesInitialization()
    {
        $reflection = new ReflectionClass($this);
        $unsetProperties = [];
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            if (!$property->isInitialized($this)) {
                $unsetProperties[] = snake_case($property->name);
            }
        }
        if (!empty($unsetProperties)) {
            throw new ModelException(
                "There are some properties not initialized: " . implode(", ", $unsetProperties),
                ModelException::UNSET_PROPERTIES,
                array_map(fn($item) => $item . '_required', $unsetProperties)
            );
        }
    }

    /**
     * @throws ModelException
     */
    private function validatePropertiesValues()
    {
        $errors = array_unique($this->validate());
        if (!empty($errors)) {
            throw new ModelException(
                "Invalid model data: " . implode(", ", $errors),
                ModelException::INVALID_DATA,
                $errors
            );
        }
    }

    /**
     * @return array
     */
    protected function validate(): array
    {
        return [];
    }

    /**
     * @param string|null $property
     * @return IModel
     */
    public function setClean(?string $property = null): IModel
    {
        if (!is_null($property) && $this->isDirty($property)) {
            unset($this->_dirtyFields[$property]);
            $this->_defaultValues[$property] = $this->$property;
            $this->_isDirty = !empty($this->_dirtyFields);
        } else {
            $this->_dirtyFields = [];
            $this->_defaultValues = $this->toArray();
            $this->_isDirty = false;
        }
        return $this;
    }

    /**
     * @param string|null $property
     * @return bool
     */
    public function isDirty(?string $property = null): bool
    {
        if ($property !== null) {
            return isset($this->_dirtyFields[$property]);
        }

        return $this->_isDirty;
    }

    /**
     * @param bool $snakeCasePropertiesName
     * @return array
     */
    public function toArray(bool $snakeCasePropertiesName = false): array
    {
        $array = [];
        foreach ($this as $key => $value) {
            if ($key[0] == '_') {
                continue;
            }
            $array[$snakeCasePropertiesName ? snake_case($key) : $key] = $value;
        }
        return $array;
    }

    /**
     * @param $name
     * @return mixed
     * @throws ModelPropertyNotFoundException
     */
    public function __get($name)
    {
        $name = camel_case($name);
        if (!$this->hasProperty($name)) {
            $this->throwPropertyNotFoundException($name);
        }
        return $this->parseFieldWhenGetting($name, $this->$name);
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * @throws ModelException
     * @throws ModelPropertyNotFoundException
     */
    public function __set($name, $value)
    {
        if ($name[0] === '_') {
            return $this->$name = $value;
        }

        $this->fill([$name => $value]);
        return $value;
    }

    /**
     * @param string $field
     * @param $value
     * @return mixed
     */
    public function parseFieldWhenGetting(string $field, $value)
    {
        return $value;
    }

    /**
     * @param bool $snakeCasePropertiesName
     * @return array
     */
    public function getDirtyFields(bool $snakeCasePropertiesName = false): array
    {
        $array = [];
        foreach ($this->_dirtyFields as $key => $value) {
            $array[$snakeCasePropertiesName ? snake_case($key) : $key] = $this->$key;
        }

        return $array;
    }

    /**
     * @return IModel
     */
    public function reset(): IModel
    {
        if (!$this->_isDirty) {
            return $this;
        }

        foreach ($this->getDirtyFieldsNames() as $field) {
            $this->$field = $this->_defaultValues[$field];
        }

        return $this;
    }

    /**
     * @param bool $snakeCasePropertiesName
     * @return array
     */
    public function getDirtyFieldsNames(bool $snakeCasePropertiesName = false): array
    {
        $keys = array_keys($this->_dirtyFields);
        if ($snakeCasePropertiesName) {
            foreach ($keys as $k => $v) {
                $keys[$k] = snake_case($v);
            }
        }

        return $keys;
    }

    /**
     * @param string $propertyName
     * @return mixed
     * @throws ModelPropertyNotFoundException
     */
    public function getDefaultValue(string $propertyName)
    {
        if (!$this->hasProperty($propertyName)) {
            $this->throwPropertyNotFoundException($propertyName);
        }
        return $this->_defaultValues[$propertyName];
    }

    /**
     * @param $value
     * @param bool $nullable
     * @return bool
     */
    protected function validateIntegerKey($value, $nullable = true): bool
    {
        if (!$nullable && is_null($value)) {
            return false;
        }
        if (!is_null($value) && (!is_integer($value) || $value < 1)) {
            return false;
        }
        return true;
    }
}
