<?php

namespace Heitus\Support;

use Heitus\Support\Contracts\IEnum;

abstract class Enum implements IEnum
{
    public static function exists($item): bool
    {
        return in_array($item, static::getAllItems(), true);
    }

    public static function getAllItems(): array
    {
        $reflection = new \ReflectionClass(static::class);
        return array_values($reflection->getConstants());
    }
}
