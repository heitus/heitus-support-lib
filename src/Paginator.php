<?php

namespace Heitus\Support;

use Heitus\Support\Contracts\IPaginator;

class Paginator implements IPaginator
{
    private int $page;
    private int $records;
    private bool $withTrashed;
    private ?string $sortBy;
    private string $sortType;
    private ?string $search;
    private ?string $filterString;

    /**
     * Paginator constructor.
     * @param int $page
     * @param int $records
     * @param bool $withTrashed
     * @param string|null $sortBy
     * @param string $sortType
     * @param string|null $search
     * @param string|null $filterString
     */
    public function __construct(
        int $page = 0,
        int $records = PHP_INT_MAX,
        bool $withTrashed = false,
        string $sortBy = null,
        string $sortType = 'ASC',
        ?string $search = null,
        ?string $filterString = null
    ) {
        $this->page = $page;
        $this->records = $records;
        $this->withTrashed = $withTrashed;
        $this->sortBy = $sortBy;
        $this->sortType = $sortType;
        $this->search = $search;
        $this->filterString = $filterString;
    }

    /**
     * @return int
     */
    public function skip(): int
    {
        return $this->page * $this->records;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function records(): int
    {
        return $this->records;
    }

    /**
     * @return bool
     */
    public function withTrashed(): bool
    {
        return $this->withTrashed;
    }

    /**
     * @return string|null
     */
    public function sortBy(): ?string
    {
        return $this->sortBy;
    }

    /**
     * @return string|null
     */
    public function sortType(): ?string
    {
        return $this->sortType;
    }

    /**
     * @return string|null
     */
    public function search(): ?string
    {
        return $this->search;
    }

    /**
     * @return string|null
     */
    public function filterString(): ?string
    {
        return $this->filterString;
    }

    /**
     * @return array|null
     */
    public function filters(): ?array
    {
        $filters = explode('|', $this->filterString);
        $result = [];
        foreach ($filters as $key => &$filter) {
            $filter = explode(':', $filter, 50);
            if (empty($filter[0]) || empty($filter[1])) {
                continue;
            }
            $key = $filter[0];
            $values = explode(';', $filter[1], 100);
            $result[$key] = [];
            foreach ($values as $value) {
                if (empty($value)) {
                    continue;
                }
                $result[$key][] = $value;
            }
            if (empty($result[$key])) {
                unset($result[$key]);
            }
        }
        return $result;
    }
}
