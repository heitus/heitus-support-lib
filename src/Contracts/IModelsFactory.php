<?php

namespace Heitus\Support\Contracts;

interface IModelsFactory
{

    /**
     * @param IRepository $targetRepository
     * @param $data
     * @return IModel
     */
    public function build(IRepository $targetRepository, $data): IModel;
}
