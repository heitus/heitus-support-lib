<?php

namespace Heitus\Support\Contracts;

interface IEnum
{
    /**
     * @param $item
     * @return bool
     */
    public static function exists($item): bool;

    /**
     * @return array
     */
    public static function getAllItems(): array;
}
