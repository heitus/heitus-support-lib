<?php

namespace Heitus\Support\Contracts;

use Heitus\Support\Exceptions\Contracts\IModelException;

interface IModel
{
    /**
     * ModelContract constructor.
     * @param \stdClass|array $stdClass
     * @throws IModelException
     */
    public function __construct($data);

    /**
     * @param \stdClass|array $data
     * @return static
     * @throws IModelException
     */
    public function fill($data): self;

    /**
     * @return static
     */
    public function reset(): self;

    /**
     * @param string|null $property
     * @return IModel
     */
    public function setClean(?string $property = null): self;

    /**
     * @param string|null $property
     * @return bool
     */
    public function isDirty(?string $property = null): bool;

    /**
     * @param bool $snakeCasePropertiesName
     * @return array
     */
    public function getDirtyFieldsNames(bool $snakeCasePropertiesName = false): array;

    /**
     * @param bool $snakeCasePropertiesName
     * @return array
     */
    public function getDirtyFields(bool $snakeCasePropertiesName = false): array;

    /**
     * @param string $propertyName
     * @return mixed
     */
    public function getDefaultValue(string $propertyName);

    /**
     * @return array
     */
    public function toArray(bool $snakeCasePropertiesName = false): array;

    /**
     * @param string $propertyName
     * @return bool
     */
    public function hasProperty(string $propertyName): bool;
}
