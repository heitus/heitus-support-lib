<?php

namespace Heitus\Support\Contracts;

interface ITenant
{

    /**
     * ITenant constructor.
     * @param string $name
     * @param $value
     */
    public function __construct(string $name, $value);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return mixed
     */
    public function getValue();
}
