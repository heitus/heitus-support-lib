<?php

namespace Heitus\Support\Contracts;

interface IPaginator
{
    /**
     * IPaginator constructor.
     * @param int $page
     * @param int $records
     * @param bool $withTrashed
     * @param string|null $sortBy
     * @param string $sortType
     * @param string|null $search
     * @param string|null $filterString
     */
    public function __construct(
        int $page = 0,
        int $records = PHP_INT_MAX,
        bool $withTrashed = false,
        string $sortBy = null,
        string $sortType = 'ASC',
        ?string $search = null,
        ?string $filterString = null
    );

    /**
     * @return int
     */
    public function skip(): int;

    /**
     * @return int
     */
    public function page(): int;

    /**
     * @return int
     */
    public function records(): int;

    /**
     * @return bool
     */
    public function withTrashed(): bool;

    /**
     * @return string|null
     */
    public function sortBy(): ?string;

    /**
     * @return string|null
     */
    public function sortType(): ?string;

    /**
     * @return string|null
     */
    public function search(): ?string;

    /**
     * @return string|null
     */
    public function filterString(): ?string;

    /**
     * @return array|null
     */
    public function filters(): ?array;
}
