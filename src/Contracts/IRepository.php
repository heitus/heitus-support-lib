<?php
declare(strict_types=1);

namespace Heitus\Support\Contracts;

use Heitus\Support\Exceptions\Contracts\IModelException;
use Illuminate\Support\Collection;

interface IRepository
{

    /**
     * @param \stdClass|array $data
     * @return IModel
     * @throws IModelException
     */
    public function makeModel($data): IModel;

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return IModel|null
     */
    public function findById($id, callable $additionalConstraints = null): ?IModel;

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return IModel|null
     */
    public function findByIdAndLockForUpdate($id, callable $additionalConstraints = null): ?IModel;

    /**
     * @param array|Collection $ids
     * @param bool $withTrashed
     * @param callable|null $additionalConstraints
     * @return Collection
     */
    public function findManyByIds(
        $ids,
        bool $withTrashed = false,
        callable $additionalConstraints = null
    ): Collection;

    /**
     * @param IPaginator $paginator
     * @param callable|null $additionalConstraints
     * @return Collection
     */
    public function getAll(
        ?IPaginator $paginator = null,
        callable $additionalConstraints = null
    ): Collection;

    /**
     * @param callable|null $additionalConstraints
     * @return IModel|null
     */
    public function first(callable $additionalConstraints = null): ?IModel;

    /**
     * @param bool $withTrashed
     * @param callable|null $additionalConstraints
     * @param IPaginator|null $paginator
     * @return int
     */
    public function countAll(
        bool $withTrashed = false,
        callable $additionalConstraints = null,
        ?IPaginator $paginator = null
    ): int;

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return bool
     */
    public function existsWithId($id, callable $additionalConstraints = null): bool;

    /**
     * @param IModel $model
     * @return IRepository
     */
    public function persist(IModel $model): self;

    /**
     * @param IModel $model
     * @param bool $setId
     * @return IRepository
     */
    public function store(IModel $model, $setId = true): IRepository;

    /**
     * @param array $modelArray
     * @param bool $setIds
     * @return IRepository
     */
    public function storeMany(array $modelArray, bool $setIds = false): self;

    /**
     * @param IModel $model
     * @return static
     */
    public function update(IModel $model): self;

    /**
     * @param array $modelsArray
     * @return IRepository
     */
    public function updateMany(array $modelsArray): self;

    /**
     * @param IModel $model
     * @return static
     */
    public function remove(IModel $model): self;

    /**
     * @param Collection $models
     * @return static
     */
    public function removeMany(Collection $models): self;

    /**
     * @param int $id
     * @param bool $forceHardDelete
     * @return $this
     */
    public function removeById(int $id, bool $forceHardDelete = false): IRepository;

    /**
     * @param array $ids
     * @param callable|null $additionalConstraints
     * @param bool $forceHardDelete
     * @return IRepository
     */
    public function removeManyByIds(
        array $ids,
        callable $additionalConstraints = null,
        bool $forceHardDelete = false
    ): self;

    /**
     * @param bool $forceHardDelete
     * @param callable|null $additionalConstraints
     * @return static
     */
    public function removeAll($forceHardDelete = false, callable $additionalConstraints = null): self;

    /**
     * @param ITenant $tenant
     * @return IRepository
     */
    public function setTenant(ITenant $tenant): IRepository;

    /**
     * @return ITenant|null
     */
    public function getTenant(): ?ITenant;

    /**
     * @return bool
     */
    public function hasTenant(): bool;
}
