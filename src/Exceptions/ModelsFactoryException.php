<?php

namespace Heitus\Support\Exceptions;

use Heitus\Support\Exceptions\Contracts\IModelsFactoryException;

class ModelsFactoryException extends \Exception implements IModelsFactoryException
{
}
