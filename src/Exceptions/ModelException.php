<?php

namespace Heitus\Support\Exceptions;

use Heitus\Support\Exceptions\Contracts\IModelException;
use Throwable;

class ModelException extends \Exception implements IModelException
{

    private array $validationErrors = [];

    /**
     * ModelException constructor.
     * @param string $message
     * @param int $code
     * @param array $errors
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = "",
        int $code = 0,
        array $validationErrors = [],
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->validationErrors = $validationErrors;
    }

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }
}
