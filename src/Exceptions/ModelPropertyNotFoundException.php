<?php

namespace Heitus\Support\Exceptions;

use Heitus\Support\Exceptions\Contracts\IModelPropertyNotFoundException;

class ModelPropertyNotFoundException extends \Exception implements IModelPropertyNotFoundException
{
}
