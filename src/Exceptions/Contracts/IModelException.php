<?php

namespace Heitus\Support\Exceptions\Contracts;

interface IModelException extends \Throwable
{
    const INVALID_PROPERTY_NAME = 1;
    const INVALID_DATA = 2;
    const UNSET_PROPERTIES = 3;

    public function __construct(string $message, int $code, array $validationErrors, \Throwable $previous = null);

    public function getValidationErrors(): array;
}
