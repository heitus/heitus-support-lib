<?php

namespace Heitus\Support\Exceptions\Contracts;

interface IRepositoryException extends \Throwable
{
}
