<?php

namespace Heitus\Support\Exceptions\Contracts;

interface IModelPropertyNotFoundException extends \Throwable
{

}
