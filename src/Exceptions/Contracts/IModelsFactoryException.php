<?php

namespace Heitus\Support\Exceptions\Contracts;

interface IModelsFactoryException extends \Throwable
{
}
