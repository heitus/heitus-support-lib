<?php

namespace Heitus\Support\Exceptions;

use Heitus\Support\Exceptions\Contracts\IRepositoryException;

class RepositoryException extends \Exception implements IRepositoryException
{
}
