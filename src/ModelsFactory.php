<?php

namespace Heitus\Support;

use Heitus\Support\Contracts\IModel;
use Heitus\Support\Contracts\IModelsFactory;
use Heitus\Support\Contracts\IRepository;
use Heitus\Support\Exceptions\Contracts\IModelsFactoryException;
use Heitus\Support\Exceptions\ModelsFactoryException;

class ModelsFactory implements IModelsFactory
{

    private static array $map = [];

    public function __construct()
    {
        self::$map = config('repositories.repositoriesToModelsMapping');
    }

    /**
     * @param IRepository $targetRepository
     * @param $data
     * @return IModel
     * @throws IModelsFactoryException
     */
    public function build(IRepository $targetRepository, $data): IModel
    {
        foreach (self::$map as $interface => $modelClass) {
            if ($targetRepository instanceof $interface) {
                return new $modelClass($data);
            }
        }

        throw new ModelsFactoryException("Model for '" . class_basename($targetRepository) . "' not mapped");
    }
}
