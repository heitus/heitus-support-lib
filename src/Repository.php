<?php

namespace Heitus\Support;

use Carbon\Carbon;
use Heitus\Support\Contracts\IModel;
use Heitus\Support\Contracts\IModelsFactory;
use Heitus\Support\Contracts\IPaginator;
use Heitus\Support\Contracts\IRepository;
use Heitus\Support\Contracts\ITenant;
use Heitus\Support\Exceptions\Contracts\IModelException;
use Heitus\Support\Exceptions\Contracts\IRepositoryException;
use Heitus\Support\Exceptions\RepositoryException;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use stdClass;

abstract class Repository implements IRepository
{
    protected ?string $tableName = null;
    protected bool $softDeletes = true;
    protected ?ITenant $tenant = null;

    protected DatabaseManager $databaseManager;
    protected Connection $connection;
    protected IModelsFactory $modelsFactory;

    protected array $sortableFields = ['id', 'createdAt', 'updatedAt'];
    protected array $searchableFields = [];
    protected array $filterableFields = [];

    /**
     * BaseRepository constructor.
     * @param DatabaseManager $databaseManager
     * @param IModelsFactory $modelsFactory
     * @throws ReflectionException
     */
    public function __construct(DatabaseManager $databaseManager, IModelsFactory $modelsFactory)
    {
        if (!$this->tableName) {
            $this->tableName = $this->guessTableName();
        }
        $this->databaseManager = $databaseManager;
        $this->connection = $databaseManager->connection();
        $this->modelsFactory = $modelsFactory;
    }

    /**
     * @return string
     * @throws ReflectionException
     */
    private function guessTableName(): string
    {
        $className = (new ReflectionClass(get_called_class()))->getShortName();
        if (Str::endsWith($className, 'Repository')) {
            return snake_case(substr($className, 0, -10));
        }
        return snake_case($className);
    }

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return IModel|null
     * @throws IModelException
     */
    public function findByIdAndLockForUpdate($id, callable $additionalConstraints = null): ?IModel
    {
        return $this->findById($id, function (Builder $query) use ($additionalConstraints) {
            $query->lockForUpdate();
            $this->applyAdditionalConstraints($query, $additionalConstraints);
        });
    }

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return IModel|null
     * @throws IModelException
     */
    public function findById($id, callable $additionalConstraints = null): ?IModel
    {
        if ($this->softDeletes) {
            $query = $this->getBuilder()->whereNull("$this->tableName.deleted_at");
        } else {
            $query = $this->getBuilder();
        }
        $this->applyAdditionalConstraints($query, $additionalConstraints);
        $stdObject = $query->where("$this->tableName.id", $id)->first();
        return $stdObject ? $this->makeModel($stdObject) : null;
    }

    /**
     * @param null $table
     * @return Builder
     */
    protected function getBuilder($table = null, $skipTenant = false): Builder
    {
        $table ??= $this->tableName;
        $builder = $this->connection->table($table);
        if ($this->hasTenant() && !$skipTenant) {
            $tenant = $this->getTenant();
            $builder->where($this->tenantTable() . '.' . snake_case($tenant->getName()), $tenant->getValue());
        }
        $builder->select("$table.*");
        return $builder;
    }

    /**
     * @return bool
     */
    public function hasTenant(): bool
    {
        return !is_null($this->tenant);
    }

    /**
     * @return ITenant|null
     */
    public function getTenant(): ?ITenant
    {
        return $this->tenant;
    }

    /**
     * @param ITenant $tenant
     * @return IRepository
     */
    public function setTenant(ITenant $tenant): IRepository
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return string
     */
    protected function tenantTable(): string
    {
        return $this->tableName;
    }

    /**
     * @param stdClass|array $data
     * @return IModel
     * @throws IModelException
     */
    public function makeModel($data): IModel
    {
        return $this->modelsFactory->build($this, $data);
    }

    /**
     * @param array|Collection $ids
     * @param bool $withTrashed
     * @param callable|null $additionalConstraints
     * @return Collection
     * @throws IRepositoryException
     */
    public function findManyByIds(
        $ids,
        bool $withTrashed = false,
        callable $additionalConstraints = null
    ): Collection {
        if (!is_array($ids) && !$ids instanceof Collection) {
            throw new InvalidArgumentException(
                'parameter $ids must be an Array or an instance of ' . Collection::class
            );
        }
        if ($ids instanceof Collection) {
            $ids = $ids->toArray();
        }
        return $this->getAll(
            null,
            function (Builder $query) use ($ids, $additionalConstraints) {
                $query->whereIn("$this->tableName.id", $ids);
                $this->applyAdditionalConstraints($query, $additionalConstraints);
            }
        );
    }

    /**
     * @param IPaginator $paginator
     * @param callable|null $additionalConstraints
     * @return Collection
     * @throws RepositoryException
     */
    public function getAll(
        ?IPaginator $paginator = null,
        callable $additionalConstraints = null
    ): Collection {

        if ($paginator === null) {
            $paginator = new Paginator();
        }

        $query = $this->getBuilder();

        $this->applyLimitConstraints($query, $paginator);
        $this->applySearchConstraints($query, $paginator);
        $this->applyFilterConstraints($query, $paginator);
        $this->applySortConstraints($query, $paginator);
        $this->applyAdditionalConstraints($query, $additionalConstraints);
        $this->applySoftDeleteConstraints($query, $paginator);

        $stdObjectCollection = $query->get();
        return $this->makeModelsFromCollection($stdObjectCollection);
    }

    /**
     * @param bool $withTrashed
     * @param callable|null $additionalConstraints
     * @param IPaginator|null $paginator
     * @return int
     */
    public function countAll(
        $withTrashed = false,
        callable $additionalConstraints = null,
        ?IPaginator $paginator = null
    ): int {
        $query = $this->getBuilder();

        $this->applyAdditionalConstraints($query, $additionalConstraints);
        $this->applySearchConstraints($query, $paginator);
        $this->applyFilterConstraints($query, $paginator);
        $this->applySoftDeleteConstraints($query, $paginator);

        return $query->count();
    }

    /**
     * @param Builder $query
     * @param IPaginator|null $paginator
     */
    private function applySoftDeleteConstraints(Builder $query, ?IPaginator $paginator = null)
    {
        if ($this->softDeletes && (is_null($paginator) || !$paginator->withTrashed())) {
            $query->whereNull("$this->tableName.deleted_at");
        }
    }

    /**
     * @param Builder $query
     * @param callable|null $additionalConstraints
     */
    protected function applyAdditionalConstraints(Builder $query, ?callable $additionalConstraints = null)
    {
        if (!is_null($additionalConstraints)) {
            $query->where($additionalConstraints);
        }
    }

    /**
     * @param Builder $query
     * @param IPaginator $paginator
     */
    private function applyLimitConstraints(Builder $query, IPaginator $paginator): void
    {
        if ($skip = $paginator->skip()) {
            $query->skip($skip);
        }
        if ($paginator->records() < PHP_INT_MAX) {
            $query->take($paginator->records());
        }
    }

    /**
     * Apply restrictions related to search that may be coming inside the paginator
     * @param Builder $query
     * @param IPaginator $paginator
     */
    private function applySearchConstraints(Builder $query, ?IPaginator $paginator = null): void
    {
        if (is_null($paginator)) {
            return;
        }
        $searchTerm = $paginator->search();
        if (empty($searchTerm) || empty($this->searchableFields)) {
            return;
        }

        $this->applySearchTableJoins($query);
        $query->where(function (Builder $query) use ($searchTerm) {
            foreach ($this->searchableFields as $field) {
                $fieldName = $this->processSearchField($field);
                $query->orWhere(
                    snake_case($fieldName),
                    'like',
                    '%' . $this->escapeLikeClause($searchTerm) . '%'
                );
            }
        });
    }

    protected function applySearchTableJoins(Builder $query): void
    {
    }

    /**
     * @param string $field
     * @return string
     */
    protected function processSearchField(string $field): string
    {
        return $field;
    }

    /**
     * @param string $value
     * @param string $char
     * @return string
     */
    private function escapeLikeClause(string $value, string $char = '\\'): string
    {
        return str_replace(
            [$char, '%', '_', ' '],
            [$char . $char, $char . '%', $char . '_', '%'],
            $value
        );
    }

    /**
     * @param Builder $query
     * @param IPaginator $paginator
     */
    private function applyFilterConstraints(Builder $query, ?IPaginator $paginator = null): void
    {
        if (is_null($paginator)) {
            return;
        }
        $filters = $paginator->filters();
        if (empty($filters) || empty($this->filterableFields)) {
            return;
        }
        foreach ($filters as $key => $values) {
            if (!in_array($key, $this->filterableFields)) {
                unset($filters[$key]);
                continue;
            }

            $query->where(function (Builder $query) use ($key, &$values) {
                foreach ($values as $value) {
                    $operatorAndValue = $this->extractFilterOperator($value);
                    $query->orWhere(
                        $this->tableName . "." . snake_case($key),
                        $operatorAndValue[0],
                        $operatorAndValue[1]
                    );
                }
            });
        }
    }

    /**
     * @param string $value
     * @return array
     */
    protected function extractFilterOperator(string $value): array
    {
        $firstChar = $value[0];
        switch ($firstChar) {
            case '!':
                return ['<>', substr($value, 1)];
            case '>':
            case '<':
            case '=':
                return [$firstChar, substr($value, 1)];
        }
        if (Str::startsWith($value, '>=')) {
            return ['>=', substr($value, 2)];
        }
        if (Str::startsWith($value, '<=')) {
            return ['<=', substr($value, 2)];
        }
        return ['=', $value];
    }

    /**
     * @param Builder $query
     * @param IPaginator $paginator
     * @throws RepositoryException
     */
    private function applySortConstraints(Builder $query, IPaginator $paginator): void
    {
        if ($sortBy = $paginator->sortBy()) {
            if (!in_array($sortBy, $this->sortableFields)) {
                throw new RepositoryException("Unsupported sort field provided to " . static::class . ": " . $sortBy);
            }
            if ($sortBy === "id") {
                $sortBy = $this->tableName . ".id";
            }
            $this->sortBy($query, $sortBy, $paginator->sortType());
        }
    }

    /**
     * @param Builder $query
     * @param string $sortBy
     * @param string $sortType
     */
    protected function sortBy(Builder $query, string $sortBy, string $sortType): void
    {
        $query->orderBy(snake_case($sortBy), $sortType);
    }

    /**
     * @param Collection $stdObjectCollection
     * @return Collection
     */
    protected function makeModelsFromCollection(Collection $stdObjectCollection): Collection
    {
        return $stdObjectCollection->transform(function ($item) {
            return $this->makeModel($item);
        });
    }

    /**
     * @param callable|null $additionalConstraints
     * @return IModel|null
     * @throws IModelException
     */
    public function first(callable $additionalConstraints = null): ?IModel
    {
        $query = $this->getBuilder();
        $this->applySoftDeleteConstraints($query);
        $this->applyAdditionalConstraints($query, $additionalConstraints);
        $stdObject = $query->first();
        return $stdObject ? $this->makeModel($stdObject) : null;
    }

    /**
     * @param $id
     * @param callable|null $additionalConstraints
     * @return bool
     */
    public function existsWithId($id, callable $additionalConstraints = null): bool
    {
        return $this->exists(function (Builder $query) use ($id, $additionalConstraints) {
            $query->where("$this->tableName.id", $id);
            $this->applyAdditionalConstraints($query, $additionalConstraints);
        });
    }

    /**
     * @param callable|null $additionalConstraints
     * @return bool
     */
    protected function exists(callable $additionalConstraints = null): bool
    {
        $query = $this->getBuilder();
        $this->applySoftDeleteConstraints($query);
        $this->applyAdditionalConstraints($query, $additionalConstraints);
        return $query->exists();
    }

    /**
     * @param IModel $model
     * @return IRepository
     * @throws RepositoryException
     */
    public function persist(IModel $model): IRepository
    {
        if (!is_null($model->id)) {
            return $this->update($model);
        } else {
            return $this->store($model);
        }
    }

    /**
     * @param IModel $model
     * @return static
     * @throws RepositoryException
     */
    public function update(IModel $model): IRepository
    {
        try {
            if ($model->isDirty()) {
                $this->fillTimestamps($model);
                $query = $this->getBuilder()->where("$this->tableName.id", $model->id);
                $this->applySoftDeleteConstraints($query);
                $data = $this->convertDatesToUTC($model->getDirtyFields(true));
                $query->update($data);
                $model->setClean();
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param array $inputData
     * @return array
     */
    private function convertDatesToUTC(array $inputData): array
    {
        foreach ($inputData as $key => $value) {
            if ($value instanceof Carbon) {
                $inputData[$key] = $value->copy()->setTimezone('UTC');
            }
        }
        return $inputData;
    }

    /**
     * @param IModel $model
     * @param bool $setId
     * @return static
     * @throws RepositoryException
     */
    public function store(IModel $model, $setId = true): IRepository
    {
        try {
            $this->fillTimestamps($model);
            $data = $this->convertDatesToUTC($model->toArray(true));
            $id = $this->getBuilder()->insertGetId($data);
            $model->id = $id;
            $model->setClean();
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param IModel $model
     */
    private function fillTimestamps(IModel $model): void
    {
        $now = Carbon::now();
        if ($model->hasProperty('created_at') && is_null($model->created_at)) {
            $model->created_at = $now->toDateTimeString();
        }
        if ($model->hasProperty('updated_at')) {
            $model->updated_at = $now->toDateTimeString();
        }
    }

    /**
     * @param array $modelArray
     * @param bool $setIds
     * @return IRepository
     * @throws RepositoryException
     */
    public function storeMany(array $modelArray, bool $setIds = false): IRepository
    {
        try {
            if ($setIds) {
                foreach ($modelArray as $model) {
                    $this->store($model);
                }
                return $this;
            }

            $dataArray = [];
            foreach ($modelArray as $model) {
                $this->fillTimestamps($model);
                $dataArray[] = $this->convertDatesToUTC($model->toArray(true));
            }
            if (!empty($dataArray)) {
                $this->getBuilder()->insert($dataArray);
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param array $modelsArray
     * @return IRepository
     * @throws RepositoryException
     */
    public function updateMany(array $modelsArray): IRepository
    {
        foreach ($modelsArray as $model) {
            $this->update($model);
        }
        return $this;
    }

    /**
     * @param IModel $model
     * @param bool $forceHardDelete
     * @return static
     * @throws RepositoryException
     */
    public function remove(IModel $model, $forceHardDelete = false): IRepository
    {
        try {
            $query = $this->getBuilder()->where("$this->tableName.id", $model->id);
            if (!$this->softDeletes || $forceHardDelete) {
                $query->delete();
            } else {
                $now = Carbon::now();
                $query->whereNull("$this->tableName.deleted_at")
                    ->update([
                        "$this->tableName.deleted_at" => $now->toDateTimeString(),
                    ]);
                $model->deletedAt = $now;
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param Collection $collection
     * @return static
     * @throws RepositoryException
     */
    public function removeMany(Collection $collection): IRepository
    {
        //Map the model ids
        $ids = $collection->map(function ($item) {
            return $item->id;
        });

        try {
            $query = $this->getBuilder()->whereIn("$this->tableName.id", $ids);
            if (!$this->softDeletes) {
                $query->delete();
            } else {
                $now = Carbon::now();
                $query->whereNull("$this->tableName.deleted_at")
                    ->update([
                        "$this->tableName.deleted_at" => $now->toDateTimeString(),
                    ]);
                foreach ($collection as $model) {
                    $model->deletedAt = $now;
                }
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param int $id
     * @param bool $forceHardDelete
     * @return IRepository
     * @throws RepositoryException
     */
    public function removeById(int $id, bool $forceHardDelete = false): IRepository
    {
        try {
            if (!$this->softDeletes || $forceHardDelete) {
                $this->getBuilder()->where("$this->tableName.id", $id)->delete();
            } else {
                $this->getBuilder()
                    ->where("$this->tableName.id", $id)
                    ->whereNull("$this->tableName.deleted_at")
                    ->update([
                        "$this->tableName.deleted_at" => Carbon::now()->toDateTimeString(),
                    ]);
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param array $ids
     * @param callable|null $additionalConstraints
     * @param bool $forceHardDelete
     * @return IRepository
     * @throws RepositoryException
     */
    public function removeManyByIds(
        array $ids,
        callable $additionalConstraints = null,
        bool $forceHardDelete = false
    ): IRepository {
        try {
            $query = $this->getBuilder()->whereIn("$this->tableName.id", $ids);
            $this->applyAdditionalConstraints($query, $additionalConstraints);
            if (!$this->softDeletes || $forceHardDelete) {
                $query->delete();
            } else {
                $query->whereNull("$this->tableName.deleted_at")->update([
                    "$this->tableName.deleted_at" => Carbon::now()->toDateTimeString(),
                ]);
            }
            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param bool $forceHardDelete
     * @param callable|null $additionalConstraints
     * @return static
     * @throws RepositoryException
     */
    public function removeAll($forceHardDelete = false, callable $additionalConstraints = null): IRepository
    {
        try {
            $query = $this->getBuilder();
            $this->applyAdditionalConstraints($query, $additionalConstraints);
            if (!$this->softDeletes || $forceHardDelete) {
                $query->delete();
            } else {
                $query->whereNull("$this->tableName.deleted_at")->update([
                    "$this->tableName.deleted_at" => Carbon::now()->toDateTimeString(),
                ]);
            }

            return $this;
        } catch (QueryException $e) {
            throw new RepositoryException($e->getMessage(), 0, $e);
        }
    }
}
