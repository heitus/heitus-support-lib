<?php

return [
    "repositoriesInterfacesToConcrete" => [
        // ISomeRepositoryInterface::class => SomeConcreteRepository::class
    ],
    "repositoriesToModelsMapping" => [
        // ISomeRepositoryInterface::class => SomeConcreteModel::class
    ],
];
