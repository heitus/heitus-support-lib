<?php

namespace Heitus\Support\Providers;

use Heitus\Support\Contracts\IModelsFactory;
use Heitus\Support\ModelsFactory;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app instanceof \Illuminate\Contracts\Foundation\Application) {
            $this->publishes([
                __DIR__ . '/../config/repositories.php' => config_path('repositories.php'),
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/repositories.php', 'repositories'
        );

        // Register factories
        $this->app->singleton(IModelsFactory::class, ModelsFactory::class);

        // Register repositories bindings
        foreach (config("repositories.repositoriesInterfacesToConcrete") as $interface => $concrete) {
            $this->app->bind($interface, $concrete);
        }
    }
}
