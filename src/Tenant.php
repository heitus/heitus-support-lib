<?php

namespace Heitus\Support;

use Heitus\Support\Contracts\ITenant;

class Tenant implements ITenant
{
    protected $name;
    protected $value;

    /**
     * ITenant constructor.
     * @param string $name
     * @param $value
     */
    public function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
